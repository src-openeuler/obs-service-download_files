Name:           obs-service-download_files
Version:        0.9.2
Release:        1
Summary:        An OBS source service: download files
License:        GPLv2+
URL:            https://github.com/openSUSE/%{name}
Source0:        https://github.com/openSUSE/%{name}/archive/%{name}-%{version}.tar.gz
BuildRequires:  perl-Git
Requires:       wget perl(YAML::XS) diffutils
BuildArch:      noarch

%description
This is a source service for openEuler Build Service.
This service downloads all source files and parses all
spec files needed for building.

%prep
%autosetup -n %{name}-%{version} -p1

%build
perl -p -i -e "s{#!/usr/bin/env bash}{#!/bin/bash}" download_files

%install
%make_install

%files
%doc README.md
%license COPYING
%config(noreplace) %{_sysconfdir}/obs/services/*
%dir %{_prefix}/lib/obs
%dir %{_sysconfdir}/obs
%dir %{_sysconfdir}/obs/services
%{_prefix}/lib/obs/service

%changelog
* Mon Oct 23 2023 Ge Wang <wang__ge@126.com> - 0.9.2-1
- Upgrade obs-server-download_files to 0.9.2

* Mon Jan 17 2022 yaoxin <yaoxin30@huawei.com> - 0.9.1-1
- Upgrade obs-service-download_files to 0.9.1

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.6.2-3
- DESC: delete -Sgit from %autosetup

* Wed Jun 23 2021 wuchaochao <wuchaochao4@huawei.com> - 0.6.2-2
- add buildrequires: perl-Git

* Wed Sep 9 2020 Ge Wang <wangge20@huawei.com> - 0.6.2-1
- Modify Source0 Url

* Sat Mar 7 2020 Shijie Luo <luoshijie1@huawei.com> - 0.6.2-0
- Package init.
